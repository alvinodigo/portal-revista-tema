<?php
/**
 * Template Name: template ciencia na ponta
 */
get_header();
if(have_posts()) : the_post();
?>
	<main role="main" class="main-main">
		<div class="container">
			<section class="page">
				<?php if ( has_post_thumbnail() ): ?>
					<h2 class="title-single"><?php the_title(); ?></h2>
					<div class="content-single">
						<img src="<?php echo get_the_post_thumbnail_url(); ?>" class="img-thumbnail" />
						<?php if( has_excerpt() ): ?>
							<div class="abstract-single">
								<?php the_excerpt(); ?>
							</div>
						<?php endif;?>
						<?php the_content(); ?>
					</div>
				<?php else:?>
					<h2 class="title-single"><?php the_title(); ?></h2>
					<div class="content-single">
						<?php if( has_excerpt() ): ?>
							<div class="abstract-single">
								<?php the_excerpt(); ?>
							</div>
						<?php endif;?>
						<?php the_content(); ?>
					</div>
				<?php endif;?>
			</section>
		</div>
	</main>
</div>
<?php
endif;
get_footer();
?>
