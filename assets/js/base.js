jQuery(document).ready(function() {
	base.acessibilidade.iniciar();
	base.acessibilidade.manipularFontes();
	base.acessibilidade.ativarAltoContraste();
	base.abas.marcar();
	base.form.validar();
	base.url.mudar();
	base.carrossel.ativar();
});

var base = {
	carrossel: {
		ativar: function() {
			var $carrossel = jQuery('.list-item--carousel');

			if ($carrossel.length > 0) {
				$carrossel.slick({
					dots: true,
					infinite: true,
					autoplay: true,
  					autoplaySpeed: 3000,
				});
			}
		}
	},

	url: {
		mudar: function() {
			jQuery('#revistas-search').submit(function(e) {
				e.preventDefault();
				var val = jQuery('#search-box__search').val();
				if (val) {
					window.location.href = portal_revista.search_target_url + '?search=' + val;
				}
				return;
			});
		}
	},

	abas: {
		ativar: function() {
			jQuery('.box-tabs__list').find('button').on('click',function() {
				var $this = jQuery(this),
					target = $this.data('target');

				$this.addClass('active').siblings().removeClass('active');

				jQuery('.box-tabs__content').find('.active').removeClass('active').end().find('.' + target).addClass('active');
			});
		},

		marcar: function() {
			var alvo = jQuery('.tab-target').data('tab-active');

			jQuery('.' + alvo).addClass('active');
		}
	},

	form: {
		validar: function() {
			jQuery('.form-search-wp').on('submit',function() {
				if (jQuery(this).find('input[type=text]').val() == '') {
					return false;
				}
			});
		}
	},

	acessibilidade: {
		iniciar: function() {
			accessibilityCounter = 0;
		},

		manipularFontes: function() {
			jQuery('.button-text-minus').on('click',function() {
				if (accessibilityCounter > -3) {
					var $html = jQuery('html'),
						fonte = $html.css('font-size'),
						tamanho = fonte.split('px');

					$html.css('font-size', (parseInt(tamanho[0]) - 3));
					accessibilityCounter--;
				}
			});

			jQuery('.button-text-default').on('click',function() {
				jQuery('html').css('font-size','1rem');
				accessibilityCounter = 0;
			});

			jQuery('.button-text-plus').on('click',function() {
				if (accessibilityCounter < 3) {
					var $html = jQuery('html'),
						fonte = $html.css('font-size'),
						tamanho = fonte.split('px');

					console.log('fonte: ',fonte);

					$html.css('font-size', (parseInt(tamanho[0]) + 3));
					accessibilityCounter++;
				}
			});
		},

		ativarAltoContraste: function() {
			jQuery('.button-high-contrast').on('click',function() {
				jQuery('body').toggleClass('contraste');
			});
		}
	}
};