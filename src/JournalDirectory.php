<?php
  namespace IBICT;
  class JournalDirectory {

    private $tainacanItems;

    public function __construct() {
      $this->tainacanItems = \Tainacan\Repositories\Items::get_instance();
      $this->load();
    }

    private function load() {
      
    }

    public function getLastUpdateItems($length, $collection) {
      $items = $this->tainacanItems->fetch(['per_page'=>$length,'orderby' => 'modified', 'order' => 'DESC'], $collection, 'OBJECT');
      return $items;
    }

  }