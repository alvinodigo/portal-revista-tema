<?php

	namespace Tainacan\Plugins;
	require_once(get_stylesheet_directory() . '/src/metadata-types/TainacanFieldsTextboxNF.php' );
	require_once(get_stylesheet_directory() . '/src/metadata-types/TainacanFieldsListSelectNF.php' );

	class TainacanNinjaForms {

		private static $instance = null;
		public static function getInstance() {
			if ( ! isset( self::$instance ) ) self::$instance = new self();
			return self::$instance;
		}

		private function __construct() {
			$this->load();
		}

		public function load() {
			if (class_exists('Ninja_Forms')) {
				add_filter( 'ninja_forms_field_type_sections', [$this, 'tainacanAddSectionNF']);
				add_filter( 'ninja_forms_register_fields', [$this, 'registerFieldsNF']);
				\Ninja_Forms::instance()->plugins_loaded();
			}
		}

		function tainacanAddSectionNF($sections) {
			//iterate over all collection and create a section for each
			$sections['tainacan'] = array(
				'id' => 'tainacan',
				'nicename' => __( 'Tainacan Metadata', 'ninja-forms' ),
				'fieldTypes' => array(),
			);
			return $sections;
		}
		
		function registerFieldsNF($fields) {
			$collection_id = 33372;
			$col = \Tainacan\Repositories\Collections::get_instance()->fetch_by_db_identifier($collection_id);
			if ($col) {
				$metadata = \Tainacan\Repositories\Metadata::get_instance()->fetch_by_collection($col, ['posts_per_page' => -1], 'OBJECT');
				foreach ($metadata as $meta) {
					$field = $this->createNewFieldNF($meta, 'T'.$meta->get_id(), $meta->get_name());
					$fields['T'.$meta->get_id()] =  $field;
				}
			}
			return $fields;
		}

		private function createNewFieldNF($metadata, $name, $label, $section='tainacan') {
			$classField = '\Tainacan\Plugins\TainacanNinjaForms\Fields\Tainacan_NF_Fields_Textbox';

			switch($metadata->get_metadata_type()) {
				case 'Tainacan\Metadata_Types\Taxonomy':
					$taxonomy_id = $metadata->get_metadata_type_options()['taxonomy_id'];
					$taxonomy = \Tainacan\Repositories\Taxonomies::get_instance()->fetch($taxonomy_id);
					$args = [
						'order' => 'desc',
						'orderby' => 'name',
						'hide_empty' => 0,
						'posts_per_page' => 20
					];
					$terms = \Tainacan\Repositories\Terms::get_instance()->fetch($args, $taxonomy);
					$options = [];
					foreach ($terms as $term) {
						$item_arr = $term->_toArray();
						$options[] = ['label' => $item_arr['name'], 'value' => $item_arr['id']];
					}
					$classField = '\Tainacan\Plugins\TainacanNinjaForms\Fields\Tainacan_NF_Fields_ListSelect';
					
					return new $classField($name, $label, $options, $section);
					break;

				default:
					return new $classField($name, $label, $section);
					break;
			}
		}
	}
	TainacanNinjaForms::getInstance();