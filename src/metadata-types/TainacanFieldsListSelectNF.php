<?php

namespace Tainacan\Plugins\TainacanNinjaForms\Fields;

class Tainacan_NF_Fields_ListSelect extends \NF_Abstracts_List {
	protected $_name = 'list';
	protected $_type = 'list';
	protected $_nicename = 'list';
	protected $_section = 'tainacan';
	
	private $options = [];

	protected $_templates = array( 'listcountry', 'listselect' );

	public function __construct($name, $label, $options, $section='tainacan') {
			parent::__construct();
			$this->_name = $name;
			$this->_type = $name;
			$this->_section = $section;
			$this->_nicename = __( $label, 'ninja-forms' );
			$this->_settings[ 'options' ][ 'group' ] = '';
			$this->options = $options;
			add_filter( 'ninja_forms_render_options_' . $this->_type, array( $this, 'filter_options'   ), 10, 2 );
	}

	public function filter_options( $options, $settings ) {
		$default_value = ( isset( $settings[ 'default' ] ) ) ? $settings[ 'default' ] : '';
		$options = $this->get_options(); // Overwrite the default list options.
		foreach( $options as $key => $option ){
			if( $default_value != $option[ 'value' ] ) continue;
			$options[ $key ][ 'selected' ] = 1;
		}
		usort( $options, array($this,'sort_options_by_label') );
		return $options;
	}

	private function sort_options_by_label( $option_a, $option_b ) {
		return strcasecmp( $option_a['label'], $option_b['label'] );
	}

	private function get_options() {
		$order = 0;
		$options = array();

		$options[] = array(
			'label' => '[' . __( 'selecione um valor', 'tainacan-ninja-forms' ) . ']',
			'value' => '',
			'calc' => '',
			'selected' => 0,
			'order' => $order,
		);

		foreach ($this->options as $option) {
			$order++;
			$options[] = array(
				'label'  => $option['label'],
				'value' => $option['value'],
				'calc' => '',
				'selected' => 0,
				'order' => $order
			);
		}
		return $options;
	}
}