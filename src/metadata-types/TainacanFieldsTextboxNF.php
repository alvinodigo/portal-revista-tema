<?php

namespace Tainacan\Plugins\TainacanNinjaForms\Fields;

class Tainacan_NF_Fields_Textbox extends \NF_Fields_Textbox {
		protected $_name = 'tainacan';
		protected $_section = 'tainacan'; // section in backend
		protected $_icon = 'text-width';
		protected $_aliases = array( 'input' );
		protected $_type = 'textbox'; // field type
		protected $_templates = 'textbox'; // template; it's possible to create custom field templates
		protected $_test_value = 'Lorem ipsum';
		protected $_settings = array(
			'disable_browser_autocomplete',
			'mask',
			'custom_mask',
			'custom_name_attribute',
			'personally_identifiable'
		);
	
		public function __construct($name, $label, $section='tainacan') {
			parent::__construct();
      $this->_name = $name;
      $this->_section = $section;
			$this->_nicename = __( $label, 'ninja-forms' );
		}
  }  