<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript"> var templateUrl = '<?= get_bloginfo("template_url"); ?>'; </script>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="<?php echo get_stylesheet_directory_uri() . '/assets/img/ico/favicon.png' ?>" rel="shortcut icon" type="image/x-icon">
		<?php wp_head(); ?>
	</head>

	<body>
		<div class="wrapper-all">
			<div class="wrapper-top">
				<div id="barra-brasil" style="background:#7F7F7F; height: 20px; padding:0 0 0 10px;display:block;">
					<ul id="menu-barra-temp" style="list-style:none;">
						<li style="display:inline; float:left;padding-right:10px; margin-right:10px; border-right:1px solid #EDEDED">
							<a href="http://brasil.gov.br" style="font-family:sans,sans-serif; text-decoration:none; color:white;">Portal do Governo Brasileiro</a>
						</li>
						<li>
							<a style="font-family:sans,sans-serif; text-decoration:none; color:white;" href="http://epwg.governoeletronico.gov.br/barra/atualize.html">Atualize sua Barra de Governo</a>
						</li>
					</ul>
				</div>

				<!-- MENU DE ACESSIBILIDADE -->
				<div class="accessibility-bar">
					<nav class="container">
						<ul class="accessibility-shortcuts" role="menubar">
							<li role="menuitem"><a href="#content" accesskey="c"><span>c</span> Ir para o conteúdo</a></li>
							<li role="menuitem"><a href="#menu-principal" accesskey="m"><span>m</span> Ir para o menu</a></li>
							<li role="menuitem"><a href="#lookfor" accesskey="b"><span>b</span> Ir para a busca</a></li>
							<li role="menuitem"><a href="#footer" accesskey="r"><span>r</span> Ir para o rodapé</a></li>
						</ul>

						<ul class="accessibility-options" role="menubar">
							<li role="menuitem">
								<span>Fonte</span>
								<button type="button" class="button-text-minus" accesskey="5">A-</button>
								<button type="button" class="button-text-default" accesskey="6">A</button>
								<button type="button" class="button-text-plus" accesskey="7">A+</button>
							</li>
							<li role="menuitem">
								<button type="button" class="button-high-contrast" accesskey="8">Alto Contraste</button>
							</li>
						</ul>
					</nav>
				</div>

				<!-- AVISO DE ERRO CASO O JS ESTEJA DESATIVADO OU NÃO ESTEJA FUNCIONANDO -->
				<noscript>
					<span>Seu navegador não tem suporte a JavaScript ou o mesmo está desativado.</span>
				</noscript>

				<header class="main-header header">
					<div class="container">

                    <!-- <h1><a href="<?php echo home_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/ciencianaponta.png'?>" alt="Ciência na Ponta"></a></h1> -->
                    <h1><a href="<?php echo home_url(); ?>">Diretório de Revistas</a></h1>

						<nav class="navbar navbar-expand-lg navbar-light navigation-menu" role="navigation">
							<div class="menu-wrapper">
								<div class="menu-wrapper__scroll">
									<button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false" aria-label="Toggle navigation">
										<span class="navbar-toggler-icon"><i class="mdi mdi-menu"></i></span>
										<i class="mdi mdi-close"></i>
									</button>

									<?php
										wp_nav_menu( array(
											'depth'             => 2,
											'container'         => 'div',
											'container_class'   => 'collapse navbar-collapse',
											'container_id'      => 'menu-principal',
											'menu_class'        => 'nav navbar-nav',
											'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback'
										) );
									?>
								</div>
							</div>
						</nav>
					</div>
				</header>