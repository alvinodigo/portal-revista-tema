			<footer role="contentinfo" class="footer" id="footer">
				<a href="#footer" id="footer" name="footer" class="sr-only">Início do rodapé</a>

				<div class="barra-logos">
					<div class="container">
						<h2 class="titulo-1">Conheça também</h2>
						<div class="barra-logos__box">
							<a class="barra-logos__logo-1" href="https://diadorim.ibict.br/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/diadorim.png'?>" alt="Diadorim"></a>
							<a class="barra-logos__logo-2" href="https://www.latindex.org" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/latindex.jpg'?>" alt="Latindex"></a>
							<a class="barra-logos__logo-3" href="http://oasisbr.ibict.br" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/oasis.png'?>" alt="Portal oasisbr"></a>
							<a class="barra-logos__logo-4" href="Portal de revistas" target="_blank"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/portal_revistas.png'?>" alt="Portal de revistas"></a>
						</div>
					</div>
				</div>

				<div class="container">
					<div class="box-logo-ibict">
						<a href="http://www.ibict.br/" target="_blank" class="logo-ibict"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/ibict.png'?>" alt="IBICT - Instituto Brasileiro de Informação em Ciência e Tecnologia"></a>
						<span>Instituto Brasileiro de Informação em Ciência e Tecnologia<br>SAUS Quadra 05 Lote 06 Bloco H – Asa Sul, Brasília, DF, 70070-912</span>
					</div>
					<a href="http://www.mctic.gov.br/portal" target="_blank" class="logo-mctic"><img src="<?php echo get_stylesheet_directory_uri() . '/assets/img/lgo/mctic.png'?>" alt="MCTIC - Ministério da ciência, tecnologia, inovações e comunicações"></a>
				</div>
			</footer>
		</div>
		<?php wp_footer(); ?>

		<script defer="defer" src="//barra.brasil.gov.br/barra.js" type="text/javascript"></script>
	</body>
</html>
