<?php
	get_header();
	$defualt_avatar = get_stylesheet_directory_uri() . '/assets/img/lgo/default_journal.jpg';
	$JD = new \IBICT\JournalDirectory();
	$journalItems = $JD->getLastUpdateItems(10,15440);
?>
		<main id="content" class="main-main">
			<div class="box-list-item">
				<ul class="list-item list-item--carousel">
					<?php foreach ($journalItems as $item):
						$thumbnail =  !empty($item->get_thumbnail()['thumbnail'][0]) ? $item->get_thumbnail()['thumbnail'][0] : $defualt_avatar;
					?>
						<li>
							<a href="cadastro.php"><img src="<?php echo $thumbnail;?>" alt="<?php echo $item->get_title(); ?>"></a>
							<div class="list-item__text">
								<strong><?php echo $item->get_title(); ?></strong>
								<p><?php echo $item->get_title(); ?></p>
								<a href="<?php echo get_permalink( $item->get_id()); ?>">Site</a>
							</div>
						</li>
					<?php endforeach; ?>
				</ul>
			</div>

			<div class="box-form-search-wp">
				<div class="container">
					<form method="get" class="form-search-wp" id="revistas-search">
						<fieldset>
							<legend>Formulário de busca</legend>

							<span class="box-form-search-wp__text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse imperdiet quam eu turpis volutpat accumsan.</span>

							<div class="form-row form-border">
								<input type="text" id="search-box__search" name="search" placeholder="Explore o acervo de revistas">
								<button type="submit"><i class="mdi mdi-magnify"></i></button>
							</div>
						</fieldset>
					</form>

					<div class="box-extra">
						<a href="#">Busca avançada</a>
					</div>
				</div>
			</div>

			<div class="box-graficos">
				<iframe
					src="http://172.25.0.76:3000/public/dashboard/33f7b54a-d05a-48ff-99ba-1245ac45d080"
					frameborder="0"
					width="800"
					height="600"
					allowtransparency
				></iframe>
			</div>
		</main>
	</div>

<?php get_footer();