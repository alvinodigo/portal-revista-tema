<?php 
function extra_files() {
	// Fontes
	wp_enqueue_style('google-custom-fonts', '//fonts.googleapis.com/css?family=Fira+Sans+Condensed:300,300i,400,400i,500,500i,600,600i,700,700i|Sansita:400,400i,700,700i');

	// CSS
	wp_enqueue_style('bootstrap-style', get_theme_file_uri() . '/assets/css/plugins/bootstrap.min.css');
	wp_enqueue_style('slick-slider-style', get_theme_file_uri() . '/assets/css/plugins/slick.css');
	wp_enqueue_style('slick-slider-theme-style', get_theme_file_uri() . '/assets/css/plugins/slick-theme.css');
	wp_enqueue_style('main-style', get_theme_file_uri() . '/assets/css/base.css', null, microtime());

	// Javascript
	//wp_enqueue_script('jquery-js', get_theme_file_uri() . '/assets/js/plugins/jquery-2.2.4.min.js', null, microtime(), true);
	wp_enqueue_script('bootstrap-js', get_theme_file_uri() . '/assets/js/plugins/bootstrap.min.js', null, microtime(), true);
	wp_enqueue_script('slick-slider-js', get_theme_file_uri() . '/assets/js/plugins/slick.min.js', null, microtime(), true);
	wp_enqueue_script('main-js', get_theme_file_uri() . '/assets/js/base.js', null, microtime(), true);

	wp_localize_script('main-js', 'portal_revista', [
		'search_target_url' => site_url('portal-de-revistas/#/')
	]);
}

function init() {
	require_once(get_stylesheet_directory() . '/src/JournalDirectory.php' );
	require_once(get_stylesheet_directory() . '/src/TainacanNinjaForms.php' );
}

add_action("init", "init");
add_action('wp_enqueue_scripts','extra_files');

add_action( 'processing_new_journal_callback', 'tainacan_NF_processing_new_journal_callback');
function tainacan_NF_processing_new_journal_callback( $form_data ) {
	$form_id       = $form_data[ 'form_id' ];
	$form_fields   =  $form_data[ 'fields' ];
	foreach( $form_fields as $field ) {
			$field_id    = $field[ 'id' ];
			$field_key   = $field[ 'key' ];
			$field_value = $field[ 'value' ];

			error_log("$field_id - $field_key - $field_value = " . json_encode($field));
	}
	$form_settings = $form_data[ 'settings' ];
	$form_title    = $form_data[ 'settings' ][ 'title' ];
}